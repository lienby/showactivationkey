# ShowActivationKey

Can give output of "Show Activation Key" option in Debug Settings- only for ANY OpenPSID you specify.

Download: https://bitbucket.org/SilicaAndPina/showactivationkey/downloads/ActivationKeyGen.exe